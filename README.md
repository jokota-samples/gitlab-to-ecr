# GitLab to ECR

GitLabのプロジェクトを、GitLab CIを使って、AWS ECRにpushするサンプルアプリケーションです。

# CloudFormation

1. GitLabユーザーを作成する
作成したユーザーのARNを確認してください。
2. ECRを作成する
GitLabユーザーのARNをパラメータに設定してください。

# expressアプリを作成する

テンプレートの作成

```
npx express-generator app
```

初期設定

```
cd app
npm install
echo node_modules > .gitignore
```

起動

```
DEBUG=myapp:* npm start
```

# Dockerfileを作成する

```
FROM node:18
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
```

# .gitlab-ci.ymlを作成する


